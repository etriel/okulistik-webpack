import _ from 'lodash';
import {
  stringify
} from 'querystring';
// import {
//   read
// } from 'fs/promises';

// import './partials/header';
require('../scss/main.scss');

function requireAll(r) {
  r.keys().forEach(r);
}

requireAll(require.context('../icons', true, /\.svg$/));

const app = {
  jsMenu() {
    const header = document.getElementsByTagName('header');
    if (header.length > 0) {
      console.log('header bu sayfada');
      var menuBtn = document.getElementsByClassName('js-menu-item')[0];
      let menuLeft = menuBtn.parentNode.getBoundingClientRect().x;
      let menuTop = menuBtn.parentNode.getBoundingClientRect().y;
      let menuHeight = menuBtn.parentNode.getBoundingClientRect().height;
      let menuCalcTop = menuTop + menuHeight;
      let jsMenuWidth = document.querySelector('.js-menu').offsetWidth;
      let jsMenuHeight = document.querySelector('.js-menu').offsetHeight;
      let $outer = document.querySelector('.js-menu-outer');
      $outer.style.left = menuLeft + 'px';
      $outer.style.top = menuCalcTop + 'px';
      $outer.style.width = jsMenuWidth + 'px';
      $outer.style.height = jsMenuHeight + 'px';

      var menuBtns = document.getElementsByClassName('js-menu-item');
      for (var i = 0; i < menuBtns.length; i++) {
        var anchor = menuBtns[i];
        anchor.onclick = function () {
          var thisMenuLeft = this.parentNode.getBoundingClientRect().x;
          var thisMenuHeight = this.parentNode.offsetHeight;
          var thisMenuTop = this.parentNode.getBoundingClientRect().y + thisMenuHeight;
          const theId = this.getAttribute('data-id');
          var thisjsMenuWidth = document.getElementById(theId).offsetWidth;
          var thisjsMenuHeight = document.getElementById(theId).offsetHeight;
          menuBtns[0].classList.remove('active');
          menuBtns[1].classList.remove('active');
          this.classList.add('active');
          document.querySelector('.overlay').classList.add('active');
          $outer.classList.add('active');
          document.getElementsByClassName('js-menu')[0].classList.remove('active');
          document.getElementsByClassName('js-menu')[1].classList.remove('active');
          document.getElementById(theId).classList.add('active');
          var thisParent = document.getElementById(theId).parentNode;
          thisParent.style.left = thisMenuLeft + 'px';
          thisParent.style.top = thisMenuTop + 'px';
          thisParent.style.width = thisjsMenuWidth + 'px';
          thisParent.style.height = thisjsMenuHeight + 'px';
        };
      }
    }
  },
  overlayFuncs() {
    if (document.getElementsByClassName('overlay').length > 0) {
      document.getElementsByClassName('overlay')[0].addEventListener('click', closeOverlay);
      document.getElementsByClassName('js-user-menu')[0].addEventListener('click', openOverlay);

      function openOverlay() {
        document.getElementsByClassName('overlay')[0].classList.add('active');
      }
    }

    function closeOverlay() {
      document.getElementsByClassName('overlay')[0].classList.remove('active');
      document.getElementsByClassName('js-menu-outer')[0].classList.remove('active');
      document.getElementsByClassName('js-menu-item')[0].classList.remove('active');
      document.getElementsByClassName('js-menu-item')[1].classList.remove('active');
      document.getElementsByClassName('js-menu')[0].classList.remove('active');
      document.getElementsByClassName('js-menu')[1].classList.remove('active');
    }
  },
  mainTab() {
    const verticalOuter = document.querySelectorAll('.js-vertical-magic-line-tab');
    if (verticalOuter.length > 0) {
      console.log('sayfada vertical outer var');
      var $magicLine = document.querySelector('.vertical-magic-line-tab');
      var magicTabActive = document.querySelector('.js-vertical-magic-line-item-tab.active');
      $magicLine.style.left = magicTabActive.offsetLeft + 'px';
      $magicLine.style.top = magicTabActive.offsetTop + 'px';
      $magicLine.style.width = magicTabActive.offsetWidth + 'px';
      $magicLine.style.height = magicTabActive.offsetHeight + 'px';

      const outerHeight = document.getElementsByClassName('js-filter-tab')[0];

      let tabBtns = document.getElementsByClassName('js-tab-item');
      for (var i = 0; i < tabBtns.length; i++) {
        var anchor = tabBtns[i];
        anchor.onclick = function () {
          var tabBtnsActive = document.querySelectorAll('.js-tab-item.active');
          for (i = 0; i < tabBtnsActive.length; i++) {
            tabBtnsActive[i].classList.remove('active');
          }
          this.classList.add('active');
          var magicTabActive = document.querySelector('.js-vertical-magic-line-item-tab.active');
          $magicLine.style.left = magicTabActive.offsetLeft + 'px';
          $magicLine.style.top = magicTabActive.offsetTop + 'px';
          $magicLine.style.width = magicTabActive.offsetWidth + 'px';
          $magicLine.style.height = magicTabActive.offsetHeight + 'px';
          var filteredItem = this.getAttribute('data-filter');
          // var filteredItemText = this.textContent;
          // document.querySelector('.js-filter-tab-title').innerHTML = filteredItemText;
          var filteredItems = outerHeight.getElementsByClassName(filteredItem);
          var allItems = outerHeight.getElementsByClassName('js-filter-tab-item');
          for (i = 0; i < allItems.length; i++) {
            allItems[i].classList.add('filtering');
          }
          setTimeout(() => {
            for (i = 0; i < allItems.length; i++) {
              allItems[i].classList.remove('filtering');
              allItems[i].classList.add('filtered');
            }
            for (i = 0; i < filteredItems.length; i++) {
              filteredItems[i].classList.remove('filtered');
            }
          }, 250);
        };
      }
    }
  },
  mainTabSimple() {
    const verticalOuter = document.querySelectorAll('.js-tab-simple-item');
    if (verticalOuter.length > 0) {
      const outerHeight = document.getElementsByClassName('js-filter-tab')[0];
      let tabSimpleBtns = document.getElementsByClassName('js-tab-simple-item');
      for (var i = 0; i < tabSimpleBtns.length; i++) {
        var anchor = tabSimpleBtns[i];
        anchor.onclick = function () {
          var tabSimpleBtnsActive = document.querySelectorAll('.js-tab-simple-item.active');
          for (i = 0; i < tabSimpleBtnsActive.length; i++) {
            tabSimpleBtnsActive[i].classList.remove('active');
          }
          this.classList.add('active');
          var filteredItem = this.getAttribute('data-filter');
          var filteredItems = outerHeight.getElementsByClassName(filteredItem);
          var allItems = outerHeight.getElementsByClassName('js-filter-tab-item');
          for (i = 0; i < allItems.length; i++) {
            allItems[i].classList.add('filtering');
          }
          setTimeout(() => {
            for (i = 0; i < allItems.length; i++) {
              allItems[i].classList.remove('filtering');
              allItems[i].classList.add('filtered');
            }
            for (i = 0; i < filteredItems.length; i++) {
              filteredItems[i].classList.remove('filtered');
            }
          }, 250);
        };
      }
    }
  },
  playVoice() {
    var sectionVoice = document.getElementsByClassName('section-voice');
    if (sectionVoice.length > 0) {
      document.getElementsByClassName('js-voice-play')[0].addEventListener('click', letplayVoice);
      var sizdenGelenlerAudio = document.getElementById('sizdenGelenlerAudio');
      var jsVoicePlay = document.querySelector('.js-voice-play');

      function letplayVoice() {
        if (jsVoicePlay.classList.contains('playing')) {
          jsVoicePlay.classList.remove('playing');
          jsVoicePlay.classList.add('stopping');
          sizdenGelenlerAudio.pause();
        } else if (jsVoicePlay.classList.contains('stopping')) {
          jsVoicePlay.classList.remove('stopping');
          jsVoicePlay.classList.add('playing');
          sizdenGelenlerAudio.play();
        } else if (jsVoicePlay.classList.contains('stopped')) {} else {
          jsVoicePlay.classList.add('playing');
          sizdenGelenlerAudio.play();
        }
      }
      sizdenGelenlerAudio.addEventListener('ended', function () {
        sizdenGelenlerAudio.currentTime = 0;
        jsVoicePlay.classList.remove('stopping');
        jsVoicePlay.classList.remove('playing');
      });
    }
  },
  modalVideoFuncs() {
    new ModalVideo('.js-video-btn', {
      channel: 'youtube',
    });
  },
  mainDropdownFuncs() {
    let dropdownBtns = document.getElementsByClassName('main-dropdown__item');
    for (var i = 0; i < dropdownBtns.length; i++) {
      var anchor = dropdownBtns[i];
      anchor.onclick = function () {
        let dropdownBtnsActive = document.querySelectorAll('.main-dropdown__item.active');
        for (var i = 0; i < dropdownBtnsActive.length; i++) {
          dropdownBtnsActive[i].classList.remove('active');
        }
        this.classList.add('active');
        var getText = this.textContent;
        this.parentNode.previousElementSibling.getElementsByClassName('main-dropdown__txt')[0].innerHTML = getText;
      };
    }
  },
  scrollFadeFunc() {
    let scrollMores = document.getElementsByClassName('scroll-more');
    for (var i = 0; i < scrollMores.length; i++) {
      var anchor = scrollMores[i];
      anchor.onscroll = function () {
        if (this.scrollTop + this.offsetHeight >= this.scrollHeight) {
          this.parentNode.classList.add('removeLinear');
          // this.parentNode.getElementsByClassName('scroll-more-btn')[0].classList.add('removeBtn');
        } else {
          this.parentNode.classList.remove('removeLinear');
          this.parentNode.classList.add('addLinear');
          // this.parentNode.getElementsByClassName('scroll-more-btn')[0].classList.remove('removeBtn');
        }
        if (this.scrollTop == 0) {
          this.parentNode.classList.remove('addLinear');
        }
      };
    }
  },
  packageSelected() {
    let packageBtns = document.getElementsByClassName('js-package-select');
    for (var i = 0; i < packageBtns.length; i++) {
      var anchor = packageBtns[i];
      anchor.onclick = function () {
        var packageBtnsActive = document.querySelectorAll('.js-package-select.active');
        for (i = 0; i < packageBtnsActive.length; i++) {
          packageBtnsActive[i].classList.remove('active');
        }
        this.classList.add('active');
      };
    }
  },
  cardJsFuncs() {
    if (document.getElementsByClassName('card-wrapper--form').length > 0) {
      new Card({
        form: '.card-form',
        container: '.card-wrapper--form',
        formSelectors: {
          numberInput: '.card__number',
          expiryInput: '.card__expiry',
          cvcInput: '.card__cvc',
          nameInput: '.card__name',
        },
        formatting: true,

        placeholders: {
          number: '1234 5678 9123 4567',
          name: 'İsim Soyisim',
          expiry: '**/**',
          cvc: '***',
        },
      });
    }
  },
  mainTooltip() {
    tippy('.ccv-tooltip', {
      content: 'Buraya Ccv nin ne olduğunu <br> açıklayan bi şey gelcek.',
      animation: 'fade',
      placement: 'right',
      allowHTML: true,
    });
    tippy('.yenikod-tooltip', {
      content: 'Kodu Tekrar Gönder.',
      animation: 'fade',
      placement: 'top',
      allowHTML: true,
    });
    tippy('.iban-tooltip', {
      content: 'İban Kopyalandı.',
      animation: 'fade',
      placement: 'top',
      trigger: 'click',
      allowHTML: true,
    });
  },
  phoneRequired() {
    if (document.getElementsByClassName('checkout-phone-required').length > 0) {
      document.querySelector('.js-next-phone-required').addEventListener('click', function () {
        this.parentNode.classList.remove('active');
        this.parentNode.nextElementSibling.classList.add('active');
      });
    }
  },
  couponFuncs() {
    if (document.getElementsByClassName('js-cupon-form').length > 0) {
      console.log('kupon bu sayfada');
      var couponInput = document.querySelector('.checkout-coupon__input');
      var someFunction = function () {
        if (this.value == '') {
          document.getElementsByClassName('cupon-error')[0].style.display = 'block';
        } else {
          document.getElementsByClassName('cupon-error')[0].style.display = 'none';
        }
      };
      document.querySelector('.js-cupon-btn').addEventListener('click', function () {
        if (couponInput.value == '') {
          document.getElementsByClassName('cupon-error')[0].style.display = 'block';
        } else {
          document.getElementsByClassName('cupon-error')[0].style.display = 'none';
          var cuponInput = this.previousElementSibling;
          document.querySelector('.js-cupon-form').classList.add('active');
          setTimeout(() => {
            document.querySelector('.js-cupon-form').classList.remove('active');
            document.querySelector('.js-cupon-form').classList.add('result');
            cuponInput.value = "";
            var z = document.createElement('div');
            z.className = 'checkout-basket__totally-item';
            z.innerHTML = '<div class="checkout-basket__totally-txt">******** Kodu İndirimi</div><div class="checkout-basket__totally-price">-₺24.90</div><div class="checkout-item__remove js-remove-sale" onclick="removeSale()"><span class="icon-outer"><svg class="icon"><use xlink:href="#icon-crash-remove"></use></svg></span></div>';
            document.querySelector('.js-discounts').appendChild(z);
          }, 3000);
          return false;
        }
      });
      couponInput.addEventListener('change', someFunction, false);
      couponInput.addEventListener('keyup', someFunction, false);
    }
  },
  mainCountFuncs() {
    var countNum = document.querySelector('.js-count-num');
    if (document.getElementsByClassName('main-count').length > 0) {
      console.log('maincount bu sayfada');
      if (countNum.value > 0) {
        document.querySelector('.js-count-up').addEventListener('click', function () {
          countNum.value = Number(countNum.value) + 1;
          if (countNum.value <= 0) {
            document.querySelector('.js-count-down').classList.add('disabled');
          } else if (countNum.value >= 0) {
            document.querySelector('.js-count-down').classList.remove('disabled');
          }
        });
        document.querySelector('.js-count-down').addEventListener('click', function () {
          countNum.value = Number(countNum.value) - 1;
          if (countNum.value <= 0) {
            document.querySelector('.js-count-down').classList.add('disabled');
          } else if (countNum.value >= 0) {
            document.querySelector('.js-count-down').classList.remove('disabled');
          }
        });
      }
    }
  },
  calendarFuncs() {
    if (document.getElementsByClassName('calendar-filter__button').length > 0) {
      var calendarButtons = document.getElementsByClassName('js-calendar-event-filter');
      for (var i = 0; i < calendarButtons.length; i++) {
        var anchor = calendarButtons[i];
        anchor.onclick = function () {
          let calendarItems = document.getElementsByClassName('calendar-month__item-cnt');
          for (var i = 0; i < calendarItems.length; i++) {
            var anchor = calendarItems[i];
            anchor.classList.add('xd');
            let calendarItemsIn = document.getElementsByClassName('js-event');
            for (var i = 0; i < calendarItemsIn.length; i++) {
              var anchorIn = calendarItemsIn[i];
              anchorIn.classList.add('xdsd');
              if (anchorIn.style.display == "none") {}
            }
          }
        };
      }
    }
  },
  init: function () {
    app.jsMenu();
    app.overlayFuncs();
    app.mainTab();
    app.playVoice();
    app.modalVideoFuncs();
    app.mainDropdownFuncs();
    app.mainTabSimple();
    app.packageSelected();
    app.cardJsFuncs();
    app.mainTooltip();
    app.couponFuncs();
    app.phoneRequired();
    app.scrollFadeFunc();
    app.mainCountFuncs();
    app.calendarFuncs();
  },
};

function ready(fn) {
  if (document.readyState != 'loading') {
    fn();
  } else {
    document.addEventListener('loaded', fn);
    console.log('loaded');
    app.init();
    return false;
  }
}
ready();


if (document.getElementsByClassName('validateForm')[0].length > 0) {
  var form = document.querySelector('[data-form]');
  var v = new vanillaValidation(form, {
    customRules: {
      valueIs: function (inputValue, ruleValue) {
        return inputValue === ruleValue;
      },
    },
    rules: {
      cname: {
        minlength: 2,
        required: true
      },
      cmail: {
        email: true,
        required: true
      },
      ctextarea: {
        required: true
      },
      cilce: {
        required: true
      },
      cpassword: {
        required: true
      },
      ctckimlik: {
        required: true
      },
    },
    messages: {
      cname: {
        minlength: 'Minimum 2 karakter.',
        required: 'Bu alan zorunludur.'
      },
      cmail: {
        email: "Geçerli bir mail adresi giriniz.",
        required: 'Bu alan zorunludur.'
      },
      ctextarea: {
        required: 'Bu alan zorunludur.'
      },
      cilce: {
        required: 'Bu alan zorunludur.'
      },
      cpassword: {
        required: 'Bu alan zorunludur.'
      },
      ctckimlik: {
        required: 'Bu alan zorunludur.'
      },
    }
  });
};